/*
** wordtab.h for 42sh in /home/camill_n/rendu/PSU_2013_42sh
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Fri Mar 14 19:11:51 2014 camill_n
** Last update Sun May 25 11:57:17 2014 camill_n
*/

#ifndef WORDTAB_H_
# define WORDTAB_H_

typedef struct	s_wd
{
  int		state;
  int		ret_c;
  int		ret_s;
  int		in_c;
  int		in_s;
  int		d;
  char		**ret;
  char		**in;
  int		cat;
  int		quote;
}		t_wd;

int	get_nb_word(char *str, char c);
void	my_free_wordtab(char **res);
void	my_show_wordtab(char **res);
int	get_sizetab(char **res);
char	**my_wordtab(char *req, char sep);
char	**duptab(char **res);
char	**quote_wt(char **in);
int	get_state(t_wd *wtab, char **in);

#endif
