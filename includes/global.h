/*
** global.h for my_select in /home/camill_n/rendu/PSU_2013_my_select
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Sat Jan 11 00:01:31 2014 Nicolas Camilli
** Last update Sun May 25 23:38:13 2014 Nicolas Camilli
*/

#ifndef GLOBAL_H_
# define GLOBAL_H_

# include <unistd.h>
# include <signal.h>
# include <stdlib.h>
# include <stdio.h>
# include <fcntl.h>
# include <string.h>
# include <strings.h>
# include <glob.h>
# include "mysh.h"
# include "alloc.h"
# include "tios.h"
# include "errors.h"
# include "list.h"
# include "read.h"
# include "utils.h"
# include "bind.h"
# include "parse.h"
# include "tree.h"
# include "wordtab.h"
# include "builtins.h"
# include "env.h"
# include "exec.h"
# include "control.h"
# include "clipboard.h"
# include "op_func.h"
# include "alias.h"
# include "init.h"

#endif
