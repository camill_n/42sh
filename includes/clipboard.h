/*
** clipboard.h for 42sh in /home/bourse_a/Documents/B2_Systeme_Unix/psu_2013_42sh
** 
** Made by BOURSET Axel
** Login   <bourse_a@epitech.net>
** 
** Started on  Fri May 16 01:09:25 2014 BOURSET Axel
** Last update Fri May 16 01:10:11 2014 BOURSET Axel
*/

#ifndef CLIPBOARD_H_
# define CLIPBOARD_H_

void	set_clip(t_data *data, int i, int j);

#endif
