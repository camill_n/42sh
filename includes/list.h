/*
** list.h for 42sh in /home/camill_n/rendu/PSU_2013_42sh
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Wed Mar 12 14:31:50 2014 camill_n
** Last update Fri May 16 05:58:12 2014 camill_n
*/

#ifndef LIST_H_
# define LIST_H_

typedef struct	s_req
{
  char		req[5000];
  int		pos;
  struct s_req	*next;
  struct s_req	*prev;
}		t_req;

typedef struct	s_cmd
{
  int		nb_elem;
  t_req		*start;
  t_req		*end;
  t_req		*current;
}		t_cmd;

int	add_cmd(t_cmd *cmd);
int	init_list(t_cmd *cmd);
int	display_list(t_data *data, char **cmd);

#endif
