/*
** utils.h for 42sh in /home/camill_n/rendu/PSU_2013_42sh
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Wed Mar 12 16:11:04 2014 camill_n
** Last update Wed Jun 11 00:03:18 2014 Maxime Boguta
*/

#ifndef UTILS_H_
# define UTILS_H_

int	my_putchar(int nb);
int	search(t_data *data, char *pattern, int mode, char *save);
int	del_char(t_req *req, int pos, int mode);
char	*my_strncat(char *str, int i, int j);
char	*insert_str(char *t, char *f, int p);
char	*my_strcut(char *s, char sep);

#endif
