/*
** read.h for 42sh in /home/camill_n/rendu/PSU_2013_42sh
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Wed Mar 12 15:21:35 2014 camill_n
** Last update Fri May 16 04:04:42 2014 camill_n
*/

#ifndef READ_H_
# define READ_H_

int	read_in(t_data *data);
int	complete(t_data *data, int mode, char *save);


#endif
