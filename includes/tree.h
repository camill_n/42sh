/*
** tree.h for mysh in /home/camill_n/rendu/PSU_2013_minishell2
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Tue Feb 11 12:10:48 2014 camill_n
** Last update Tue Jun 10 23:14:47 2014 Maxime Boguta
*/

#ifndef TREE_H_
# define TREE_H_

# define NRIGHT (node->right)
# define NLEFT (node->left)
# define REQ (node->req)
# define OP (node->op)
# define LEFT_T (node->left->op)
# define RIGHT_T (node->right->op)

typedef struct	s_exec
{
  char		*sym;
  int		op;
  int		(*exec)(t_data *data, t_node *node, int mode);
  struct s_exec	*next;
}		t_exec;

typedef struct	s_node
{
  char		*req;
  int		op;
  struct s_node	*left;
  struct s_node	*right;
}		t_node;

t_node	*set_node(char *req, char **env);
int	free_tree(t_node *node);
int	disp_tree(t_node *node, char **op);
void	add_exec(t_data *data, int op, int (*exec)(t_data *data,
						   t_node *node, int mode), char *sym);

#endif
