/*
** errors.h for my_select in /home/camill_n/rendu/PSU_2013_my_select
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Sun Jan  5 16:02:32 2014 Nicolas Camilli
** Last update Fri May 23 15:00:20 2014 camill_n
*/

#ifndef ERRORS_H_
# define ERRORS_H_

# define MALLOC_ERROR 0
# define TIOS_GET_ERROR 1
# define TIOS_SET_ERROR 2
# define SYNTAX_ERROR 3

int	displayError(int err_code, char *info);

#endif
