/*
** mysh.h for 42sh in /home/camill_n/rendu/PSU_2013_42sh
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Wed Mar 12 01:50:51 2014 camill_n
** Last update Wed Jun 11 01:23:14 2014 Maxime Boguta
*/

#ifndef MYSH_H_
# define MYSH_H_

# define TERM data->term

# define CMD data->cmd
# define CUR_CMD data->cmd->current
# define NB_OP		8
# define SP		1
# define DAND		2
# define DOR		3
# define LEFT		5
# define PIPE		4
# define DLEFT		6
# define DRIGHT		7
# define RIGHT		8


typedef struct termios t_termios;
typedef struct s_cmd t_cmd;
typedef struct s_cap t_cap;
typedef struct s_bind t_bind;
typedef struct s_built t_built;
typedef struct s_node t_node;
typedef struct s_exec t_exec;
typedef struct s_alias t_alias;

typedef struct	s_term
{
  char		*clip;
  t_termios	*tios;
  t_termios	*tios_save;
  int		W;
  int		H;
}		t_term;

typedef struct	s_data
{
  int		run;
  t_term	*term;
  t_exec	*exec;
  t_alias	*alias;
  t_cmd		*cmd;
  char		*ps1;
  t_cap		*cap;
  char		**env;
  int		env_size;
  t_built	*built;
  t_bind	*bind;
  char		*pwd;
  char		buff[5];
  int		lastexit;
}		t_data;

int	shell(t_data *data);
int	disp_ps1();
int	set_char(t_data *data, char c);
int	my_putchar(int nb);
char	*my_strcat(char *s1, char *s2);
int	catch_children(int status);
void	check_killer(char *path, char **cmd);
void	init_bashrc(char *f, t_data *a);
int	my_echo(t_data *data, char **cmd);

#endif
