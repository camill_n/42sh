/*
** control.h for 42sh in /home/bourse_a/Documents/B2_Systeme_Unix/psu_2013_42sh/includes
** 
** Made by BOURSET Axel
** Login   <bourse_a@epitech.net>
** 
** Started on  Tue May 13 16:46:35 2014 BOURSET Axel
** Last update Tue May 13 16:48:50 2014 BOURSET Axel
*/

#ifndef CONTROL_H_
# define CONTROL_H_

int     ctrl_a(t_data *data);
int     ctrl_e(t_data *data);
int     ctrl_u(t_data *data);
int     ctrl_k(t_data *data);

#endif
