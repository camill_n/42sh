/*
** init.h for 42sh in /home/camill_n/rendu/PSU_2013_42sh
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Tue May 20 08:42:47 2014 camill_n
** Last update Tue May 20 08:46:16 2014 camill_n
*/

#ifndef INIT_H_
# define INIT_H_

void	init_data(t_data *data, t_cmd *cmd, t_cap *cap, char **env);
void	init_cap(t_cap *cap);

#endif
