/*
** parse.h for mysh in /home/camill_n/rendu/PSU_2013_minishell2
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Tue Feb 11 16:20:21 2014 camill_n
** Last update Fri May 23 21:36:28 2014 Maxime Boguta
*/

#ifndef PARSE_H_
# define PARSE_H_

int	parse(t_data *data, char *l);
int	init_op(t_data *data);
int	get_op_pos(char *, int *, t_exec *exec);

#endif
