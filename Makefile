##
## Makefile for test in /home/camill_n/rendu/Piscine-C-lib/my
##
## Made by Nicolas Camilli
## Login   <camill_n@epitech.net>
##
## Started on  Mon Oct 21 07:17:19 2013 Nicolas Camilli
## Last update Tue Jun 10 23:25:23 2014 Maxime Boguta
##

SRCS =	src/main.c \
	src/termios.c \
	src/shell.c \
	src/list.c \
	src/read.c \
	src/signal.c \
	src/init.c \
	src/exec/tree.c \
	src/exec/exec.c \
	src/exec/parse.c \
	src/exec/op.c \
	src/exec/op_func.c \
	src/exec/op_func2.c \
	src/utils/utils.c \
	src/utils/wordtab.c \
	src/utils/wordtab2.c \
	src/utils/getcfg.c \
	src/utils/str.c \
	src/utils/str2.c \
	src/utils/alloc.c \
	src/builtins/env.c \
	src/builtins/env2.c \
	src/builtins/chdir.c \
	src/builtins/builtins.c \
	src/builtins/alias.c \
	src/bind/clipboard.c \
	src/bind/bind_ctrl.c \
	src/bind/bind_ctrl2.c \
	src/bind/bind_ctrl3.c \
	src/bind/bind.c \
	src/bind/bind2.c \
	src/bind/bind3.c \

RM =	rm -f

NAME =	mysh

AR =	ar rc

CC =	cc -Wall -g3

OBJS =	$(SRCS:.c=.o)

CFLAGS = -I./includes

all:	$(NAME)

.c.o:
	$(CC) $(CFLAGS) -c $< -o $(<:.c=.o)

$(NAME): $(OBJS)
	 $(CC) $(OBJS) -o $(NAME) -lncurses

clean:
	$(RM) $(OBJS)

fclean:	clean
	$(RM) $(NAME)

re: fclean all
