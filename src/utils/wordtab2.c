/*
** wordtab2.c for mysh in /home/camill_n/rendu/PSU_2013_minishell2
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Tue Feb 18 19:03:13 2014 camill_n
** Last update Wed Jun 11 01:52:21 2014 Maxime Boguta
*/

#include <string.h>
#include <stdlib.h>
#include "alloc.h"
#include "wordtab.h"

char	*my_strncat(char *str, int i, int j)
{
  char	*new;
  int	k;

  new = x_malloc(((j - i) + 1) * sizeof(char), "new");
  k = 0;
  while (i < j)
    {
      new[k] = str[i];
      ++i;
      ++k;
    }
  new[k] = '\0';
  return (new);
}

char	**my_wordtab(char *av, char sep)
{
  char	**tab;
  int	nb_word;
  int	i;
  int	tmp;
  int	cpt;

  nb_word = get_nb_word(av, ':');
  tab = x_malloc((nb_word + 1) * sizeof(char *), "tab");
  i = 0;
  cpt = 0;
  while (av[i] != '\0')
    {
      while ((av[i] == ' ' || av[i] == '\t' || av[i] == sep) && av[i] != '\0')
        ++i;
      tmp = i;
      while (av[tmp] != '\0' && (av[tmp] != sep &&
				 av[tmp] != ' ' && av[tmp] != '\t'))
        ++tmp;
      tmp - i > 0 ? tab[cpt] = my_strncat(av, i, tmp) : 0;
      tmp - i > 0 ? ++cpt : 0;
      i = tmp;
    }
  tab[cpt] = NULL;
  return (tab);
}

void	cat_wd(t_wd *wt)
{
  int	size;
  int	i;
  int	j;
  char	*r;

  i = -1;
  j = 0;
  size = strlen(wt->ret[wt->d]) + strlen(wt->in[wt->in_s]) + 2;
  r = x_malloc(size, "wtab");
  bzero(r, size);
  strcpy(r, wt->ret[wt->d]);
  strcat(r, " ");
  j = strlen(r);
  while (wt->in[wt->in_s][++i])
    {
      if (wt->in[wt->in_s][i] != '"')
	r[j++] = wt->in[wt->in_s][i];
      else
	wt->state = !wt->state;
    }
  free(wt->ret[wt->d]);
  r[j] = 0;
  wt->ret[wt->d] = r;
  wt->ret_s--;
  wt->cat = 1;
}

void	set_up_wt(t_wd *wt)
{
  while (wt->in[++wt->in_s])
    {
      wt->ret_c = -1;
      wt->in_c = -1;
      wt->cat = 0;
      wt->quote = 0;
      while (wt->in[wt->in_s][++wt->in_c] && wt->cat == 0)
	{
	  if (wt->state && wt->d != wt->in_s && wt->quote != 1)
	    cat_wd(wt);
	  else if (wt->in[wt->in_s][wt->in_c] == '"')
	    {
	      if (!wt->state)
		{
		  wt->d = wt->ret_s;
		  wt->quote = 1;
		}
	      wt->state = !wt->state;
	    }
	  else
	    wt->ret[wt->ret_s][++wt->ret_c] = wt->in[wt->in_s][wt->in_c];
	}
      wt->ret_s++;
    }
}

char	**quote_wt(char **in)
{
  int	i;
  t_wd	wtab;

  i = -1;
  if ((wtab.state = get_state(&wtab, in)) == 1)
    return (NULL);
  wtab.ret = x_malloc((get_sizetab(in) + 2) * sizeof(char *), "wtab");
  while (in[++i])
    {
      wtab.ret[i] = x_malloc(sizeof(char) * (strlen(in[i]) + 1), "wtab");
      bzero(wtab.ret[i], strlen(in[i]) + 1);
    }
  wtab.state = wtab.state;
  wtab.in = in;
  wtab.in_s = -1;
  wtab.ret_s = 0;
  set_up_wt(&wtab);
  my_free_wordtab(in);
  while (wtab.ret_s < i)
    free(wtab.ret[--i]);
  wtab.ret[wtab.ret_s] = NULL;
  return (wtab.ret);
}
