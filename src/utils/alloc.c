/*
** alloc.c for lib in /home/camill_n/rendu/PSU_2013_my_select/lib/src
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Wed Jan  8 18:13:56 2014 Nicolas Camilli
** Last update Wed Mar 12 01:22:43 2014 camill_n
*/

#include "global.h"

void	*x_malloc(int size, char *name_var)
{
  void	*var;

  var = malloc(size);
  if (var == NULL)
    displayError(MALLOC_ERROR, name_var);
  return (var);
}
