/*
** getcfg.c for 42sh in /home/boguta_m/rendu/psu_2013_42sh
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Thu May 22 17:32:57 2014 Maxime Boguta
** Last update Wed Jun 11 00:46:39 2014 Maxime Boguta
*/

#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "global.h"

char		*parse_line(char *l)
{
  char		*r;
  int		i;

  i = -1;
  r = NULL;
  while (l && l[++i] && l[i] != '#' && l[i] != '\n')
    {
      r = realloc(r, i + 2);
      r[i] = l[i];
    }
  if (r)
    r[i] = 0;
  if (l)
    free(l);
  return (r);
}

void		get_entries(FILE *fd, t_data *a)
{
  char		*line;
  size_t	i;

  line = NULL;
  while (getline(&line, &i, fd) != 0 && line[0] != 0)
    {
      if ((line = parse_line(line)))
	parse(a, line);
      free(line);
      line = NULL;
    }
}

void		init_bashrc(char *f, t_data *a)
{
  FILE		*fd;

  del_env(a->env, "PS1");
  if ((fd = fopen(f, "r")) == NULL)
    {
      write(2, "Error : Can't open configuration file\n", 32);
      return ;
    }
  get_entries(fd, a);
}
