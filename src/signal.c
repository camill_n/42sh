/*
** signal.c for 42sh in /home/camill_n/rendu/PSU_2013_42sh
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Mon May 19 08:23:08 2014 camill_n
** Last update Sun May 25 23:10:38 2014 Nicolas Camilli
*/

#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include "wordtab.h"

int	catch_children(int status)
{
  if (WIFSIGNALED(status) && WTERMSIG(status) == SIGSEGV)
    printf("segmentation fault (core dumped)\n");
  return (WIFEXITED(status) ? 0 : -1);
}
