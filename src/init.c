/*
** init.c for 42sh in /home/camill_n/rendu/PSU_2013_42sh
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Tue May 20 08:37:48 2014 camill_n
** Last update Wed Jun 11 01:26:28 2014 Maxime Boguta
*/

#include "global.h"

void	init_data(t_data *data, t_cmd *cmd, t_cap *cap, char **env)
{
  data->exec = NULL;
  data->alias = NULL;
  data->lastexit = 0;
  data->run = 1;
  data->ps1 = "$> ";
  data->cmd = cmd;
  data->env = duptab(env);
  if (data->env == NULL || !data->run)
    return ;
  data->env_size = get_sizetab(data->env);
  data->cap = cap;
  bzero(data->buff, 5);
}

void	init_cap(t_cap *cap)
{
  tgetent(NULL, "xterm");
  cap->us = tgetstr("us", NULL);
  cap->kl = tgetstr("dl", NULL);
  cap->ue = tgetstr("ue", NULL);
  cap->cl = tgetstr("cl", NULL);
  cap->so = tgetstr("so", NULL);
  cap->se = tgetstr("se", NULL);
  cap->ti = tgetstr("ti", NULL);
  cap->te = tgetstr("te", NULL);
  cap->cm = tgetstr("cm", NULL);
  cap->le = tgetstr("le", NULL);
  cap->nd = tgetstr("nd", NULL);
  cap->ec= tgetstr("ec", NULL);
  cap->ec= tgetstr("dc", NULL);
}
