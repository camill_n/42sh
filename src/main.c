/*
** main.c for 42sh in /home/camill_n/rendu/PSU_2013_42sh
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Fri Mar 14 01:33:21 2014 camill_n
** Last update Wed Jun 11 21:31:00 2014 BOURSET Axel
*/

#include "global.h"

t_data		data;

int		disp_ps1()
{
  int		i;
  char		*f;

  f = NULL;
  i = strlen(data.cmd->current->req);
  tputs(data.cap->kl, 1, &my_putchar);
  data.pwd = NULL;
  data.pwd = getcwd(data.pwd, 200);
  if ((f = get_env(data.env, "PS1")) == NULL)
    printf("$ (%s) > %s", data.pwd, data.cmd->current->req);
  else
    printf("%s (%s)> %s", (f = get_env(data.env, "PS1")), data.pwd,
	   data.cmd->current->req);
  write(1, "\r", 1);
  fflush(stdout);
  while (i > data.cmd->current->pos)
    {
      tputs(data.cap->le, 1, &my_putchar);
      --i;
    }
  free(f);
  return (0);
}

int		displayError(int err_code, char *info)
{
  if (err_code == MALLOC_ERROR)
    printf("Memory access denied for var: %s\n", info);
  if (err_code == TIOS_GET_ERROR)
    printf("tcgetattr failled.\n");
  if (err_code == TIOS_SET_ERROR)
    printf("tcsetattr failled.\n");
  if (err_code == SYNTAX_ERROR)
    printf("Syntax error after: %s.\n", info);
  data.run = -1;
  return (0);
}

void		ctrl_c(int sig)
{
  printf("^C");
  fflush(stdout);
  write(1, "\n", 1);
  bzero(data.cmd->current->req, sizeof(data.cmd->current->req));
  data.cmd->current->pos = 0;
  disp_ps1();
}

int		destruct(t_termios *tios_save)
{
  int		i;
  t_req		*tmp;
  t_req		*tmp2;
  t_built	*built;

  i = 0;
  tmp = data.cmd->start;
  built = data.built;
  while (i < data.cmd->nb_elem)
    {
      tmp2 = tmp->next;
      free(tmp);
      tmp = tmp2;
      ++i;
    }
  set_termi(tios_save);
  my_free_wordtab(data.env);
  while (built)
    {
      free(built->cmd);
      built = built->next;
    }
  return (0);
}

int		main(int ac, char **av, char **env)
{
  t_termios	tios;
  t_termios	tios_save;
  t_term	term;
  t_cmd		cmd;
  t_cap		cap;

  signal(SIGINT, ctrl_c);
  init_data(&data, &cmd, &cap, env);
  data.run == 1 ? init_tios(&data, &term, &tios, &tios_save) : 0;
  data.run == 1 ? init_list(&cmd) : 0;
  data.run == 1 ? init_bind(&data) : 0;
  data.run == 1 ? init_op(&data) : 0;
  data.run == 1 ? init_cap(&cap) : 0;
  data.run == 1 ? init_built(&data) : 0;
  data.run == 1 ? init_bashrc(".myshrc", &data) : 0;
  data.run == 1 ? shell(&data) : 0;
  data.run == 0 ? destruct(&tios_save) : 0;
  return (data.lastexit);
}
