/*
** op.c for op in /home/camill_n/rendu/PSU_2013_42sh
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Wed Apr 30 14:57:22 2014 camill_n
** Last update Tue Jun 10 18:32:27 2014 Maxime Boguta
*/

#include "global.h"

int		init_op(t_data *data)
{
  add_exec(data, DAND, logic_func, "&&");
  add_exec(data, DOR, logic_func, "||");
  add_exec(data, DLEFT, dleft_func, "<<");
  add_exec(data, DRIGHT, right_func, ">>");
  add_exec(data, SP, NULL, ";");
  add_exec(data, LEFT, left_func, "<");
  add_exec(data, PIPE, pipe_func, "|");
  add_exec(data, RIGHT, right_func, ">");
  return (0);
}

int		get_op_pos(char *str, int *type, t_exec *exec)
{
  char		*occ;
  int		pos;
  int		i;

  i = 0;
  pos = -1;
  while (exec)
    {
      if ((occ = strstr(str, exec->sym)) != NULL)
      	{
	  if ((exec->op < *type && occ[0] != occ[1]) || *type == -1)
	    {
	      *type = exec->op;
	      pos = occ - str;
	    }
	}
      ++i;
      exec = exec->next;
    }
  return (*type == -1 ? -1 : pos);
}

int		is_redir(char *str)
{
  int		ret;

  ret = 0;
  if (str && str[0] != 0)
    {
      strcmp(str, "<") == 0 ? ret = 1 : 0;
      strcmp(str, "<<") == 0 ? ret = 1 : 0;
      strcmp(str, ">") == 0 ? ret = 1 : 0;
      strcmp(str, ">>") == 0 ? ret = 1 : 0;
    }
  return (ret);
}
