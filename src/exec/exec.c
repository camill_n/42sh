/*
** exec.c for 42sh in /home/camill_n/rendu/PSU_2013_42sh
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Thu May  1 12:06:42 2014 camill_n
** Last update Wed Jun 11 01:58:10 2014 Maxime Boguta
*/

#include <sys/types.h>
#include <sys/wait.h>
#include "global.h"

int	find_exec_path(char **env, char *cmd_name, char **buff)
{
  int	i;
  char	**tabl;
  char	*path_line;
  char	tmp[250];

  if (buff[0] == NULL && access(cmd_name, X_OK) == 0)
    buff[0] = strdup(cmd_name);
  path_line = get_env(env, "PATH");
  if (buff[0] == NULL && path_line != NULL)
    {
      tabl = my_wordtab(path_line, ':');
      i = 0;
      while (env != NULL && tabl[i] != NULL && buff[0] == NULL)
	{
	  bzero(tmp, 250);
	  strcat(tmp, tabl[i]);
	  strcat(tmp, "/");
	  strcat(tmp, cmd_name);
	  access(tmp, X_OK) == 0 ? buff[0] = strdup(tmp) : 0;
	  ++i;
	}
      my_free_wordtab(tabl);
    }
  free(path_line);
  return (buff[0] == NULL ? 0 : 1);
}

int		my_fork(char *path, char **cmd, char **env, t_data *data)
{
  pid_t		pid;
  int		status;

  if ((pid = vfork()) == 0)
    {
      if (!strcmp(path, "/usr/bin/kill") || !strcmp(path, "/bin/kill"))
      	setsid();
      execve(path, cmd, env);
      exit(0);
    }
  waitpid(pid, &status, 0);
  data->lastexit = WEXITSTATUS(status);
  return (catch_children(status));
}

int		my_exec(t_node *node, t_data *data)
{
  char		**cmd;
  char		*cmd_path;
  int		ret;

  cmd_path = NULL;
  cmd = my_wordtab(node->req, ' ');
  cmd = quote_wt(cmd);
  if (cmd == NULL)
    return (EXIT_FAILURE);
  check_alias(data, &cmd);
  ret = find_builtins_exec(data, cmd);
  if (ret)
    return (builtins_exec(data, cmd));
  if (find_exec_path(data->env, cmd[0], &cmd_path))
    ret = my_fork(cmd_path, cmd, data->env, data);
  else if (!(find_builtins_exec(data, cmd)))
    {
      printf("No command found for: %s\n", cmd[0]);
      my_free_wordtab(cmd);
      return (-1);
    }
  my_free_wordtab(cmd);
  free(cmd_path);
  return (ret);
}

int		exec_node(t_data *data, t_node *node)
{
  t_exec	*exec;
  int		op;

  if (node == NULL || !data->run)
    return (0);
  exec = data->exec;
  while (exec)
    {
      if (node->op > 1 && exec->op == node->op)
	{
	  op = (node->op == DRIGHT) ? 1 : 0;
     	  return (exec->exec(data, node, op));
     	}
      exec = exec->next;
    }
  if (node->op < 2 && node != NULL)
    {
      if (node->op == 0)
	return (my_exec(node, data));
      exec_node(data, NLEFT);
      exec_node(data, NRIGHT);
    }
  return (0);
}
