/*
** op_func.c for mysh in /home/camill_n/rendu/PSU_2013_minishell2
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Thu Feb 13 14:35:11 2014 camill_n
** Last update Wed Jun 11 11:11:47 2014 camill_n
*/

#include <sys/wait.h>
#include <errno.h>
#include "global.h"

char		*get_output_name(t_data *data, t_node *node)
{
  char		**tabl;
  char		*output;
  t_node	*tmp;

  if (!NRIGHT->op)
    output = NRIGHT->req;
  else
    {
      printf("on cherche\n");
      tmp = NRIGHT;
      while (tmp)
	{
	  if (!tmp->op)
	    output = tmp->req;
	  tmp = tmp->right;
	}
    }
  tabl = my_wordtab(output, ' ');
  return (tabl[0]);
}

int	pipe_action(int *pipefd, int *save, int fd)
{
  int	r;

  r = !fd;
  close(pipefd[fd]);
  *save = dup(r);
  dup2(pipefd[r], r);
  return (0);
}

int	pipe_func(t_data *data, t_node *node, int mode)
{
  int	pipefd[2];
  int	ret;
  int	pid;
  int	save;
  int	status;

  ret = pipe(pipefd);
  if (ret == -1)
    return (-1);
  if ((pid = fork()) == 0)
    {
      pipe_action(pipefd, &save, 0);
      ret = exec_node(data, NLEFT);
      dup2(save, 1);
      exit(ret);
    }
  else
    {
      pipe_action(pipefd, &save, 1);
      exec_node(data, NRIGHT);
      dup2(save, 0);
      waitpid(pid, &status, WNOHANG);
    }
  return (0);
}

int	right_func(t_data *data, t_node *node, int mode)
{
  int	fd;
  int	save;
  char	**tabl;
  int	flag;
  char	*output;

  output = get_output_name(data, node);
  printf("output: %s\n", output);
  flag = (!mode) ? O_TRUNC : O_APPEND;
  tabl = my_wordtab(NRIGHT->req, ' ');
  fd = open(output, O_CREAT | flag | O_RDWR, 0644);
  if (fd == -1)
    return (-1);
  save = dup(1);
  dup2(fd, 1);
  exec_node(data, NLEFT);
  dup2(save, 1);
  close(fd);
  my_free_wordtab(tabl);
  return (0);
}

int	left_func(t_data *data, t_node *node, int mode)
{
  int		fd;
  int		save;
  char		**tabl;

  if (!NRIGHT)
    return (-1);
  tabl = my_wordtab(NRIGHT->req, ' ');
  fd = open(tabl[0], O_RDONLY);
  if (fd == -1)
    {
      printf("The file doesn't exist %s\n", tabl[0]);
      return (-1);
    }
  save = dup(0);
  dup2(fd, 0);
  exec_node(data, NLEFT);
  dup2(save, 0);
  close(fd);
  return (0);
}
