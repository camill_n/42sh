/*
** tree.c for 42sh in /home/boguta_m/rendu/42sh/src/exec
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Wed Jun 11 00:29:56 2014 Maxime Boguta
** Last update Wed Jun 11 00:29:57 2014 Maxime Boguta
*/

#include "global.h"

t_node		*set_node(char *req, char **env)
{
  t_node	*node;
  char		*fenv;
  char		*ask;
  int		i;

  i = -1;
  fenv = NULL;
  node = x_malloc(sizeof(t_node), "node");
  node->op = 0;
  node->right = NULL;
  node->left = NULL;
  while (req && req[++i])
    if (req[i] == '$' && req[i + 1] != 0 && req[i + 1] != ' ')
      {
	ask = my_strcut(&req[i + 1], ' ');
	fenv = get_env(env, ask);
	free(ask);
	if (fenv)
	  {
	    req = insert_str(req, fenv, i);
	    free(fenv);
	  }
      }
  node->req = strdup(req);
  return (node);
}

int		free_tree(t_node *node)
{
  if (node != NULL)
    {
      OP == 0 ? free(REQ) : 0;
      free_tree(NRIGHT);
      free_tree(NLEFT);
      free(node);
    }
  return (0);
}

int		disp_tree(t_node *node, char **op)
{
  if (node != NULL)
    {
      if (OP == 0)
	printf("Noeud de op req: %s\n", REQ);
      else
	printf("Noeud de type : %d ()\n", node->op);
      disp_tree(NLEFT, op);
      disp_tree(NRIGHT, op);
    }
  return (0);
}

void		add_exec(t_data *data, int op,
			 int (*exec)(t_data *data, t_node *node, int mode), char *sym)
{
    t_exec	*tmp;
    t_exec	*new_exec;

    new_exec = x_malloc(sizeof(t_exec), "exec");
    new_exec->op = op;
    new_exec->exec = exec;
    new_exec->sym = strdup(sym);
    new_exec->next = NULL;
    if (data->exec == NULL)
      data->exec = new_exec;
    else
      {
	tmp = data->exec;
        while (tmp->next != NULL)
	  tmp = tmp->next;
        tmp->next = new_exec;
      }
}
