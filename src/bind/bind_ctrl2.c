/*
** bind_ctrl2.c for 42sh in /home/camill_n/rendu/PSU_2013_42sh
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Tue May 20 08:49:40 2014 camill_n
** Last update Sun May 25 23:37:00 2014 Nicolas Camilli
*/

#include "global.h"

int		alt_f(t_data *data)
{
  int		i;
  int		check;
  int		size;

  i = CUR_CMD->pos;
  size = strlen(CUR_CMD->req);
  check = 0;
  while (check != 2 && i != size)
    {
      if (check == 0)
	if (CUR_CMD->req[i + 1] >= 33 && CUR_CMD->req[i + 1] <= 126)
	  ++check;
      if (check == 1)
	if (CUR_CMD->req[i + 1] == ' ')
	  ++check;
      i++;
    }
  CUR_CMD->pos = i;
  return (0);
}

int		ctrl_t(t_data *data)
{
  char		c;

  if (CUR_CMD->pos == 0)
    return (0);
  if (CUR_CMD->pos == strlen(CUR_CMD->req))
    {
      c = CUR_CMD->req[CUR_CMD->pos - 2];
      CUR_CMD->req[CUR_CMD->pos - 2] = CUR_CMD->req[CUR_CMD->pos - 1];
      CUR_CMD->req[CUR_CMD->pos - 1] = c;
      return (0);
    }
  c = CUR_CMD->req[CUR_CMD->pos - 1];
  CUR_CMD->req[CUR_CMD->pos - 1] = CUR_CMD->req[CUR_CMD->pos];
  CUR_CMD->req[CUR_CMD->pos] = c;
  CUR_CMD->pos++;
  return (0);
}

int		alt_b(t_data *data)
{
  int		i;
  int		check;

  i = CUR_CMD->pos;
  check = 0;
  while (check != 2 && i != 0)
    {
      if (check == 0)
	if (CUR_CMD->req[i - 1] >= 33 && CUR_CMD->req[i - 1] <= 126)
	  ++check;
      if (check == 1)
	if (CUR_CMD->req[i - 1] == ' ')
	  ++check;
      check == 2 ? 0 : i--;
    }
  CUR_CMD->pos = i;
  return (0);
}

int		alt_c(t_data *data)
{
  int		i;
  int		check;
  int		size;

  i = CUR_CMD->pos;
  size = strlen(CUR_CMD->req);
  check = 0;
  while (check != 2 && i != size)
    {
      if (!check && CUR_CMD->req[i] >= 33 && CUR_CMD->req[i] <= 126)
	{
	  if (CUR_CMD->req[i] >= 97 && CUR_CMD->req[i] <= 122)
	    CUR_CMD->req[i] -= 32;
	  check++;
	}
      if (check == 1)
	{
	  CUR_CMD->req[i] == ' ' ? ++check : 0;
	  if (CUR_CMD->req[i] >= 65 && CUR_CMD->req[i] <= 90)
	    CUR_CMD->req[i] = CUR_CMD->req[i] + 32;
	}
      check == 2 ? 0 : i++;
    }
  CUR_CMD->pos = i;
  return (0);
}

int		verif_bind(t_data *data, char *save)
{
  t_bind	*bind;
  int		ret;

  ret = 0;
  bind = data->bind;
  while (bind)
    {
      if (bind->size == 1 && bind->save[0] == 9)
	{
	  save[2] != 9 && save[3] == 9 && ++ret ? complete(data, 0, save) : 0;
	  save[2] == 9 && save[3] == 9 && ++ret ? complete(data, 1, save) : 0;
	  save[2] == 9 && save[3] == 9 ? bzero(save, 5) : 0;
	  if (ret)
	    return (1);
	}
      if (verif_utils(bind->save, save, bind->size))
      	{
	  bind->exec(data);
	  bzero(save, 5);
      	  return (1);
      	}
      bind = bind->next;
    }
  return (0);
}
