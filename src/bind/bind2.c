/*
** bind2.c for 42sh in /home/camill_n/rendu/PSU_2013_42sh
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Fri May 16 02:31:30 2014 camill_n
** Last update Sun May 25 10:51:09 2014 camill_n
*/

#include "global.h"

int	keyup(t_data *data)
{
  if (CUR_CMD != CMD->start)
    CUR_CMD = CUR_CMD->prev;
  return (0);
}

int	keydown(t_data *data)
{
  if (CUR_CMD != CMD->end)
    CUR_CMD = CUR_CMD->next;
  return (0);
}

int	keyright(t_data *data)
{
  if (CUR_CMD->pos < strlen(CUR_CMD->req))
    ++CUR_CMD->pos;
  return (0);
}

int	keyleft(t_data *data)
{
  if (CUR_CMD->pos > 0)
    --CUR_CMD->pos;
  return (0);
}
