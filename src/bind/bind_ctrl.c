/*
** bind_ctrl.c for 42sh in /home/camill_n/rendu/PSU_2013_42sh
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Tue May 20 08:49:15 2014 camill_n
** Last update Sun May 25 23:17:14 2014 Nicolas Camilli
*/

#include "global.h"

int	ctrl_a(t_data *data)
{
  CUR_CMD->pos = 0;
  return (0);
}

int	ctrl_l(t_data *data)
{
  tputs(CAP->cl, 1, my_putchar);
  return (0);
}

int	ctrl_e(t_data *data)
{
  CUR_CMD->pos = strlen(CUR_CMD->req);
  return (0);
}

int	ctrl_u(t_data *data)
{
  int	size;
  int	i;

  size = strlen(CUR_CMD->req);
  i = CUR_CMD->pos;
  if (CUR_CMD->pos == 0)
    return (0);
  if (size == CUR_CMD->pos)
    {
      set_clip(data, 0, CUR_CMD->pos);
      bzero(CUR_CMD->req, CUR_CMD->pos + 1);
      CUR_CMD->pos = 0;
      return (0);
    }
  set_clip(data, 0, CUR_CMD->pos);
  while (i != 0)
    del_char(CUR_CMD, i--, 0);
  return (0);
}

int	ctrl_k(t_data *data)
{
  int	size;
  int	i;

  size = strlen(CUR_CMD->req);
  i = CUR_CMD->pos;
  set_clip(data, i, size);
  while (i != size)
    {
      CUR_CMD->req[i] = 0;
      i++;
    }
  return (0);
}
