/*
** chdir.c for 42sh in /home/camill_n/rendu/PSU_2013_42sh
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Mon May 19 12:05:25 2014 camill_n
** Last update Wed Jun 11 22:28:36 2014 BOURSET Axel
*/

#include <errno.h>
#include "global.h"

void		my_strcatenv(t_data *data, char *name, char *path)
{
  char		*new;

  if (!path)
    return ;
  new = x_malloc(sizeof(char) * (strlen(name) + strlen(path) + 1), "cd");
  bzero(new, sizeof(char) * (strlen(name) + strlen(path) + 1));
  strcat(new, name);
  strcat(new, path);
  data->env = mysetenv(data->env, new);
  free(new);
}

void		init_tmp(t_data *data, char **cmd, char **tmp)
{
  char		*latest;

  tmp[0] = get_env(data->env, "HOME");
  tmp[0] == NULL ? tmp[0] = strdup("/") : 0;
  if (cmd[1] && !strcmp(cmd[1], "~"))
    return ;
  latest = get_env(data->env, "OLDPWD");
  cmd[1] != NULL ? tmp[0] = strdup(cmd[1]) : 0;
  if (cmd[1] != NULL && !strcmp(cmd[1], "-"))
    {
      if (latest == NULL)
	{
	  printf("OLDPWD unfound\n");
	  tmp[0] = NULL;
	}
      latest != NULL ? tmp[0] = strdup(latest) : 0;
    }
  latest != NULL ? free(latest) : 0;
}

int		my_chdir(t_data *data, char **cmd)
{
  char		*tmp;
  int		ret;

  ret = 0;
  init_tmp(data, cmd, &tmp);
  if (tmp != NULL && chdir(tmp) == -1)
    {
      if (errno == EACCES)
	printf("Permission denied for: %s\n", tmp);
      else
	printf("mysh: cd: %s: Directory name doesn't exist.\n", tmp);
      --ret;
    }
  else if (tmp != NULL)
    {
      del_env(data->env, "OLDPWD");
      del_env(data->env, "PWD");
      my_strcatenv(data, "PWD=", tmp);
      my_strcatenv(data, "OLDPWD=", data->pwd);
    }
  tmp != NULL && cmd[1] == NULL ? free(tmp) : 0;
  return (ret);
}

int		format_tab(t_data *data, char ***new, char ***cmd)
{
  int		size;
  int		tmp;
  int		i;

  i = 1;
  size = get_sizetab(*new);
  tmp = size;
  size += get_sizetab(*cmd);
  *new = realloc(*new, (size + 1) * sizeof(char *));
  while (tmp < size)
    {
      new[0][tmp] = cmd[0][i];
      ++tmp;
      ++i;
    }
  new[0][tmp] = NULL;
  cmd[0] = new[0];
  return (0);
}
