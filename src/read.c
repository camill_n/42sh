/*
** read.c for 42sh in /home/camill_n/rendu/PSU_2013_42sh
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Wed Mar 12 15:21:51 2014 camill_n
** Last update Tue Jun 10 22:00:14 2014 Maxime Boguta
*/

#include "global.h"

int	complete(t_data *data, int mode, char *save)
{
  int	i;
  int	j;
  int	size;
  char	*tmp;

  j = CUR_CMD->pos;
  i = j;
  size = strlen(CUR_CMD->req);
  while (i > 0 && CUR_CMD->req[i - 1] != ' ' && CUR_CMD->req[i - 1] != '\t')
    --i;
  while (j < size && (CUR_CMD->req[j] != ' ' && CUR_CMD->req[j] != '\t'))
    ++j;
  tmp = x_malloc(((j - i) + 2) * sizeof(char), "tmp");
  memcpy(tmp, CUR_CMD->req + i, (j - i));
  tmp[j - i] = 0;
  search(data, tmp, mode, save);
  free(tmp);
  return (0);
}

int	set_char(t_data *data, char c)
{
  int	size;

  size = strlen(CUR_CMD->req);
  if (CUR_CMD->pos > 4998)
    return (0);
  if (CUR_CMD->pos == size)
    CUR_CMD->req[CUR_CMD->pos] = c;
  else
    {
      ++size;
      while (size >= CUR_CMD->pos)
	{
	  CUR_CMD->req[size + 1] = CUR_CMD->req[size];
	  --size;
	}
      CUR_CMD->req[CUR_CMD->pos] = c;
    }
  ++CUR_CMD->pos;
  return (0);
}

int	save_char(char *save, char *buff)
{
  save[0] = save[1];
  save[1] = save[2];
  save[2] = save[3];
  save[3] = *buff;
  if (save[0] == '\033' || save[1] == '\033' || save[2] == '\033')
    *buff = -1;
  return (0);
}

int	read_in(t_data *data)
{
  char	buff;
  char	save[5];

  buff = -1;
  bzero(save, 5);
  while (read(0, &buff, 1) > 0 && buff != '\004' && buff != '\n')
    {
      save_char(save, &buff);
      verif_bind(data, save);
      if (buff != -1)
	buff > 31 && buff < 127 ? set_char(data, buff) : 0;
      disp_ps1();
    }
  buff == '\004' ? data->run = 0 : 0;
  write(1, "\n", 1);
  if (strlen(CUR_CMD->req) > 0 && data->run)
    {
      parse(data, NULL);
      if (CUR_CMD == CMD->end)
	add_cmd(CMD);
      else
	CUR_CMD = CMD->end;
    }
  return (0);
}
