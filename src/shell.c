/*
** shell.c for 42sh in /home/camill_n/rendu/PSU_2013_42sh
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Wed Mar 12 02:11:30 2014 camill_n
** Last update Fri May 23 14:36:28 2014 camill_n
*/

#include "global.h"

int		get_term_size(t_data *data)
{
  t_winsize	size;

  ioctl(0, TIOCGWINSZ, (char*)&size);
  TERM->W = size.ws_col;
  TERM->H = size.ws_row;
  return (0);
}

int		shell(t_data *data)
{
  while (data->run)
    {
      get_term_size(data);
      disp_ps1();
      read_in(data);
    }
  return (0);
}
